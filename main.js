$( document ).ready(function() {
  let swapper = {}
  let swapping = false
  let puppies = [{ credit_url: 'https://www.google.com/search?tbs=sbi:AMhZZisI68QTQ16DHZ1WyoNa4_1FAhAojg71JyjAgBkoPuMTIeMRwbOAO0LT_1fNd50RI-T1Ms1YOj92RSBrEZ1cigaZh8-RIMJ7Y5ooVwdxkl6IJf_16v78ufFUOVwG1IFHYz3r2umJuRdhyvSQO7xP5aTwFJJjwHKexCOWMiuw1yyDOcKNTMnhir688APlNUs9dzrZydcqjJbEtFmZg0w4OmKvSriHwS01sBbuhmGUFSuH8nSBjjf4jYER6s1Htvqnnb23xvScdQPe3g7ivRq93IPjSXxkB_1pSBpcy4Qx9e5L2Eza66uI0Mi0megPjc0rMEml7KiynTChV7GTj3j5Nb69mrCkF0KvTA',
                    image_url: 'http://cdn1-www.dogtime.com/assets/uploads/gallery/30-impossibly-cute-puppies/impossibly-cute-puppy-2.jpg' },
                 { credit_url: 'https://www.google.com/search?tbs=sbi:AMhZZiufFYqsaur502KJm-gm73N7U4zToSMK66jM3sJOWEpUPEV4ZWBj-JwMeq5eRgJ9W56W10p7AFxUpskNKxj5o4jxHd2ojKro9MRxZJMP9aRWY-41FraOKMXYRoiquQ6x7X1Br3GxkLpHaXoy5997nzQDXWgW46MdZlD91DEObCJyAvt7hFRIyQd2XLnPk1LSipueH8xuOhkiNn4Mq9z1GWdIQUtFvrByoY-ItOifOMswZS6r0C6xkiuQ3d9Opq5G1OEo99N3G6IsLLjgcP7l2t3S2G5Q8foDLMCyx35h-RKSBx2GC9Yj-Ze2Mv1briSCLi_1iHIO9EenQ4nqs_1X8pGDQ7AzzyHQ',
                    image_url: 'https://i.ytimg.com/vi/PXkp4vmJth8/maxresdefault.jpg' },
                 { credit_url: 'https://www.google.com/search?tbs=sbi:AMhZZitAhCcKk7qPURRoN4WgfQgsYcA8NYS2YWaN_1MvJdhcMEfEzov5Jn1tEHna6V16vjgrgZHzCesr0jUj5mja7ExAV9kq5T2Re7Qpt9QEJXmAM-HmKM-L3uLOm1AaVXnNSrsxGsFgkkTkrCoo8McKQM1M_1W6HLDEg_1is4OJwNONy4-yPEPUhE8ZOQlMxEj-Q-C41RnbYpUJ1ImtxscnOGXWaw14agbMVFVvxfAqaUa3SlCyI9jrc_1O1scU6JUnM99i-dlAyNxlu0Wrj4UdNJiqYzRk_1D15Bm6cu0ZgGdi_1pl1KRLXefLHlCROiXY79mobr-esc0lFNnTfZw3rXrWSMMhWi84Y3Vw',
                    image_url: 'http://vetcetera.ca/wp-content/uploads/2016/03/puppy.jpg' },
                 { credit_url: 'https://www.google.com/search?tbs=sbi:AMhZZitd9CT9gG4lO6NP6PfEQY7VVanZHI9DsTwwTGZBo5SGcAy-IbXgR2yFqnHpTiMik6KUeLUDqZsPU3m8CgWmsjwpSEjD_1qtgnsYNKTPumZ6V96yJlZI6YicSoc0KXfHz-eiziX7sKc45kDLsRpQoX00iHogKj29LbtxZWexjWrwrZoGMtDMZDZ1Xa351PGbPLaLo5PMTLPtIAR2e9yP5Vj8qG4U_1VqRxcDhsgB_1bb_1s6K5E2IHM7Tzoe1-mSWaXPrGCFZA1D8EcQCHQkiUcY_1pVd01-RdmlDQcoBHjOXMXvDxNscd3WI45ebfz9oAmvLGNdMTqjSMgsSd_1iM4TVBpVv5YAGDrQ',
                    image_url: 'https://i.ytimg.com/vi/oGoPUw0YBAg/maxresdefault.jpg' },
                 { credit_url: 'https://www.google.com/search?tbs=sbi:AMhZZisXfzVB-2ALFWcmWMw5m86Sl_1qwryoYl7pSEta8GiUxtP9Ts702bbQvKw2ItxEKtxNTystImKz6xBbiD9t3tpSWUr-5gEJCcHTFvtBQObohCMRiBLQgVDbsPbdu4SMZn2vwxXdCTsJdAG8YNUVC-umGNjbJacIRnsJN1ia2tp5fwfriV65eMUiUmPPm25lkCX-PBrZqg3FFme303WM2V7nzKWENQzI4ZjHPSE8tnm3E04-oIrHSZsTlVIMdrMLrbMj1Bu9mbldG80Z7ETRm9Pc7HSH1TrS4_1kraCOlvP-dKBnp8rWBZfOBkdc-PYxai0JpH8Fu47i6TIe2L5GjHkPLRKpvR1w',
                    image_url: 'http://www.dogs-lovers.com/wp-content/uploads/2016/11/dog_puppy_spotted_bench_105023_1920x1080.jpg' }]

  $('#the_credit').attr('href', puppies[3].credit_url)
  $('#the_image_holder').css('background-image', 'url(' + puppies[3].image_url + ')')

  $('#the_button').click(function() {
    if (swapping){
      clearInterval(swapper)
      $('.photo-credit').addClass('show')
    } else {
      swapper = setInterval(swapPuppy, 200)
      $('.photo-credit').removeClass('show')
    }
    swapping = !swapping
  })

  function swapPuppy(){
    i = Math.floor(Math.random() * puppies.length)
    $('#the_image_holder').css('background-image', 'url(' + puppies[i].image_url + ')')
    $('#the_credit').attr("href", puppies[i].credit_url)
  }

})
